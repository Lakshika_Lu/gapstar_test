 $(document).ready(function() {
    load_url = "http://127.0.0.1:8000/api/chart_data";
    $.ajax({
        url: load_url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {         
            drawOnboardChart(data);
        }
    });
  });

//Draw onboard chart  
function drawOnboardChart(chart_data) {
    Highcharts.chart('chart_div', {
        title: {
          text: chart_data.title.text
        },
        subtitle: {
          text: chart_data.subtitle.text
        },            
        yAxis: {
          title: {
            text: chart_data.yAxis.title.text,
            format: chart_data.yAxis.labels.format
          }
        },
        xAxis: {
          categories:chart_data.xAxis.categories,          
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },  
/*
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false
            },
            pointStart: 2016
          }
        },  
        */    
        series:chart_data.series,      
        responsive: {
          rules: [{
            condition: {
              maxWidth: 500
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
              }
            }
          }]
        }      
      });
 }
