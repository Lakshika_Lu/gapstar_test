<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\OnboardModel;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;


class OnboardController extends Controller {

    //Draw Onboard chart
    function getChartData() {  
            $weekly_anlysis = OnboardModel::select([
                            DB::raw('DATE_ADD(created_at, INTERVAL(2-DAYOFWEEK(created_at)) DAY) AS week_start'),
                            DB::raw('CONCAT(YEAR(created_at), "/", WEEK(created_at)) AS week_name'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage <= 100 THEN 1 ELSE 0 END) AS Step1'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 0 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step2'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 20 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step3'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 40 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step4'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 50 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step5'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 70 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step6'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage > 90 AND onboarding_perentage <= 99.99 THEN 1 ELSE 0 END) Step7'),
                            DB::raw('SUM(CASE WHEN onboarding_perentage = 99.99 THEN 1 ELSE 0 END) Step8')
                        ])
                        ->groupBy('week_name')
                        ->orderBy(DB::raw('YEAR(created_at)'),'ASC')
                        ->orderBy(DB::raw('WEEK(created_at)'),'ASC')
                        ->get();
            $chart_data ["chart"] = array (
                "type" => "line"
            );
            $chart_data ["title"] = array (
                "text" => "Onboard Weekly Retention Curve"
            );
            $chart_data ["subtitle"] = array (
                "text" => "Onboard Analysis"
            );             
            $chart_data ["xAxis"] = array (
                "categories" => array ()
            );
            $chart_data ["tooltip"] = array (
                "valueSuffix" => "%"
            );
            $category_arr = array ('0', '20', '40', '50', '70', '90', '99', '99.99' );
            $chart_data ["xAxis"] = array (
                "categories" => $category_arr
            );
            $chart_data ["yAxis"] = array (
                "title" => array (
                    "text" => "Total Onboarded"
                ),
                'labels' => array(
                    'format' => '{value}%'
                ),
                'min' => '0',
                'max' => '100'
            );
            foreach ($weekly_anlysis as $week){
                $data_arr = array();
                for($i = 1; $i <= 8; $i++) {
                    if($i == 1) {
                        $data_arr[] = 100;
                    } else {
                        $data_arr[] = round(($week->{"Step".$i}/$week->Step1) * 100);
                    }
                }
                $chart_data ["series"] [] = array (
                    "name" => $week->week_start,
                    "data" => $data_arr
                );   
            }
            return response()->json($chart_data)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function index() {      
        return view('chart');
    }


    public function showChart() {
        return view('showChart');
    }

}
