<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class OnboardModel extends Model {
    
    protected $table      = 'onboarding';  
    public    $timestamps = false; 

}
